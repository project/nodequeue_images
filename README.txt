* Introduction
---------------
This module allows user to use any of the attached image field
with nodes to show up along with titles for node selection in
nodequeues. This module will be helpful when we have multiple
nodes  with same name so to distinguish with attached image media.
Consider of adding movies node to your playlist nodequeue, as
there can be movies with same name so attached image will help
user to opt which node to add.

User has option to configure the image style for the image to
show in autocomplete nodequeues suggestion. User also has option
 to choose which image field will be used to show in nodequeues.

* Visit the sandbox page
  https://www.drupal.org/sandbox/arpitr/2354879


* Requirements
--------------
This module requires the following modules:

* Nodequeue (https://www.drupal.org/project/nodequeue) (>=7.x-3.x)


* Installation
--------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


* Configuration
  Simply go and start creating the nodeqeueus, while creating a
  nodequeue you will see option to choose content type checkbox
  option once selecting the content type you will see list of
  all file fields/image field attached to the content type as
  radio buttons, selecting any image field will allow you to
  configure the image style you want to have on image display.

  Once configured you will see the attached images to nodes
  along with title on autocomplete suggestion of nodequeues.


MAINTAINERS
-----------

Current maintainers:
* Arpit Rastogi (arpitr) - https://www.drupal.org/user/2354838
